// ---------------------------------------------------------------------------
//  encode-form-data
//
//  prepare form data for submission
// ---------------------------------------------------------------------------

    function encodeFormData(string) {
        return encodeURIComponent(string).replace(/%20/g, '+');
    }

    module.exports = encodeFormData;
